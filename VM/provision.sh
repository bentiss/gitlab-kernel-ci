#!/bin/bash

# For these command to be run properly, you need to unprotect the VM (RW, wan
# access) this script must be run as root

if [[ $EUID -ne 0 ]]
then
   echo "This script must be run as root"
   exit 1
fi

# exit the script if any command fails
set -e -x

# update the system
dnf update -y

# remove PackageKit, we don't want it to be run in background
dnf remove -y PackageKit || echo .

# install required packages
dnf install -y virt-manager virt-install libvirt-nss libvirt-client \
               anaconda-tui pykickstart fedora-kickstarts livecd-tools \
               tmux

# add libvirt_guest to nsswitch.conf to be able to ssh@VM_name
sed -i -E ' /^hosts:.*libvirt_guest.*$/b;
           s/^hosts:(\s*files)(.*)$/hosts:\1 libvirt_guest\2/' \
          /etc/nsswitch.conf

# create a tmpfs dir for gitlab-runner
if [ ! -e /app ]
then
  mkdir -p /app
  echo 'tmpfs	/app	tmpfs	nodev	 0	0' >> /etc/fstab
fi
mount -a

# install/update gitlab-runner

# stop it first and uninstall it in case it's running
if [ -e /usr/local/bin/gitlab-runner ] 
then
  gitlab-runner stop
  rm /usr/local/bin/gitlab-runner
fi
curl -o /usr/local/bin/gitlab-runner \
     https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

chmod +x /usr/local/bin/gitlab-runner

gitlab-runner install --user=root --working-directory=/app || echo .

gitlab-runner start

# create a ssh key in case we don t'have one
if [ ! -e /root/.ssh/id_rsa ]
then
  ssh-keygen -t rsa -f /root/.ssh/id_rsa -N ''
fi

# create the live VM disk that we will be using
ksflatten --config VM/workstation.ks -o workstation-flatten.ks
SSH_KEY_OF_THE_HOST=$(cat /root/.ssh/id_rsa.pub)
sed -i "s|SSH_KEY_OF_THE_HOST|$SSH_KEY_OF_THE_HOST|g" workstation-flatten.ks

IMAGE_NAME="runner-live-$(date +%Y%m%d-%H%M).qcow2"

# disable selinux as asked by livemedia-creator
setenforce 0
livemedia-creator --no-virt                    \
                  --ks workstation-flatten.ks  \
                  --make-disk --qcow2          \
                  --image-name=$IMAGE_NAME     \
                  --resultdir images

# reenable selinux
setenforce 1

mv images/* /var/lib/libvirt/images
rmdir images

# delete any existing runner VM
virsh undefine runner-live-install || echo .
virsh undefine runner-live || echo .

# delete any previous references to runner-live* in known_hosts
sed -i -E '/^runner-live.*$/d' /root/.ssh/known_hosts

# now create the runner VM with disk rw to be able to set up ssh keys
virt-install --name runner-live-install                               \
             --os-type=Linux --os-variant=fedora-unknown              \
             --ram=2048                                               \
             --vcpus=$(nproc)                                         \
             --import                                                 \
             --disk=/var/lib/libvirt/images/${IMAGE_NAME},bus=virtio  \
             --graphics=spice                                         \
             --network network=default                                \
             --noautoconsole

# make sure we can ssh to it as root (poll for 1 minute)
set +x
for run in {1..20}
do
  sleep 3
  ssh -o StrictHostKeyChecking=accept-new runner-live-install echo connected 2>/dev/null && break || echo -n .
done || exit 1
set -x

mkdir -p /var/lib/libvirt/kernels
scp runner-live-install:/boot/vmlinuz\*.x86_64 runner-live-install:/boot/initramfs\*.x86_64.img /var/lib/libvirt/kernels
eval $(ssh runner-live-install blkid -o export)

VMLINUZ=$(ls -dt /var/lib/libvirt/kernels/vmlinuz-* | head -1)
ln -sf $VMLINUZ /var/lib/libvirt/kernels/vmlinuz
INITRD=$(ls -dt /var/lib/libvirt/kernels/initramfs-* | head -1)
ln -sf $INITRD /var/lib/libvirt/kernels/initramfs.img

# properly shut down the VM and unregister it
virsh shutdown runner-live-install
set +x
for run in {1..20}
do
  sleep 3
  virsh undefine runner-live-install 2>/dev/null && break || echo -n .
done || exit 1
set -x

# delete any previous references to runner-live-install* in known_hosts
sed -i -E '/^runner-live-install.*$/d' /root/.ssh/known_hosts

# now create the runner VM eith ro disk
virt-install --name runner-live                                                \
             --os-type=Linux --os-variant=fedora-unknown                       \
             --ram=2048                                                        \
             --vcpus=$(nproc)                                                  \
             --import                                                          \
             --disk=/var/lib/libvirt/images/${IMAGE_NAME},bus=virtio,readonly=on \
             --graphics=spice                                                  \
             --network network=default                                         \
             --boot kernel=/var/lib/libvirt/kernels/vmlinuz,initrd=/var/lib/libvirt/kernels/initramfs.img,kernel_args="root=UUID=$UUID ro rhgb console=tty0 console=ttyS0 no_timer_check" \
             --noautoconsole

set +x
for run in {1..20}
do
  sleep 3
  ssh -o StrictHostKeyChecking=accept-new runner-live echo connected 2>/dev/null && break || echo -n .
done || exit 1
set -x
virsh shutdown runner-live

gitlab-runner register --name vm_controller_$(hostname) \
                       --url https://gitlab.banquise.eu \
                       --tag-list vm                    \
                       --executor shell

# https://docs.fedoraproject.org/f28/install-guide/appendixes/Kickstart_Syntax_Reference.html

# Configure installation method
install
%include /usr/share/spin-kickstarts/fedora-repo.ks

# zerombr
zerombr

# Configure Boot Loader
bootloader --location=mbr --driveorder=sda


# Remove all existing partitions
clearpart --all

# Create Physical Partition
part / --size 5120 --fstype ext4
#part /boot --size=512 --asprimary --ondrive=sda --fstype=xfs
#part swap --size=10240 --ondrive=sda $fdepass
#part / --size=8192 --grow --asprimary --ondrive=sda --fstype=xfs

# Configure Firewall
firewall --enabled --ssh
services --enabled=NetworkManager,sshd

# Configure Network Interfaces
network --bootproto=dhcp --device=link --activate

# Configure Keyboard Layouts
keyboard us

# Configure Language During Installation
lang en_US

# Configure X Window System
#xconfig --startxonboot

# Configure Time Zone
timezone Europe/Paris

# Configure Authentication
auth --passalgo=sha512

# Create User Account
user --name=liveuser --password=live --groups=wheel

# Set Root Password
rootpw --lock

# Perform Installation in Text Mode
#text

# Package Selection
%packages
-bluez
-blueberry
-dnfdragora
-gssproxy
@core
git
jq
python3-pytest
python3-pyudev
libevdev
%end

# Post-installation Script
%post

# mount /app and /usr/local as tmpfs to have a proper writable working directory
mkdir /app
cat >> /etc/fstab <<EOF
tmpfs	/app		tmpfs	nodev	0 0
tmpfs	/usr/local	tmpfs	nodev	0 0
EOF

# Disable services
systemctl disable sssd
systemctl disable bluetooth.target
systemctl disable avahi-daemon
systemctl disable abrtd
systemctl disable abrt-ccpp
systemctl disable mlocate-updatedb
systemctl disable mlocate-updatedb.timer


# install the ssh key from the HOST
mkdir /root/.ssh
chmod -m0700 /root/.ssh

cat <<EOF > /root/.ssh/authorized_keys
SSH_KEY_OF_THE_HOST
EOF

chmod 0600 /root/.ssh/authorized_keys
restorecon -R /root/.ssh/

%end

# Shutdown After Installation
shutdown

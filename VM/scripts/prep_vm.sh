#!/bin/bash
# SPDX-License-Identifier: GPL-2.0

set -e

# first build with the default selftests kernel settings

SCRIPT_DIR="$(dirname $(realpath $0))"
PREP_KERNEL_CHECKOUT="$(realpath ${SCRIPT_DIR}/../../../)"
TEST_DIR=${PREP_KERNEL_CHECKOUT}/tools/testing/selftests/hid

PLATFORM="$(uname -m)"
case "${PLATFORM}" in
x86_64)
	BZIMAGE="arch/x86/boot/bzImage"
	ARCH="x86_64"
	TARGET="bzImage"
	;;
aarch64)
	BZIMAGE="arch/arm64/boot/Image"
	ARCH="arm64"
	TARGET="Image"
	;;
*)
	echo "Unsupported architecture"
	exit 1
	;;
esac

while getopts 'm' opt; do
  case ${opt} in
  m)
    modules="yes"
    unset TARGET
    ;;
  : )
    echo "Invalid Option: -$OPTARG requires an argument"
    exit 1
    ;;
  esac
done
shift $((OPTIND -1))

echo $SCRIPT_DIR
ls $SCRIPT_DIR
echo "---"
echo $PREP_KERNEL_CHECKOUT
ls $PREP_KERNEL_CHECKOUT
echo "---"
echo $TEST_DIR
ls $TEST_DIR

git checkout ${TEST_DIR}/config

cat <<EOF > $TEST_DIR/config.lima
EOF

if [ "$PLATFORM" == "aarch64" ]
then
  cat <<EOF > $TEST_DIR/config.lima
CONFIG_BLK_DEV_LOOP=y
CONFIG_EROFS_FS=y
CONFIG_EROFS_FS_ONDEMAND=y
CONFIG_EROFS_FS_ZIP_DEFLATE=y
CONFIG_EROFS_FS_ZIP_LZMA=y
CONFIG_EROFS_FS_ZIP_ZSTD=y
CONFIG_FAT_DEFAULT_IOCHARSET="ascii"
CONFIG_HW_RANDOM=y
CONFIG_HW_RANDOM_VIRTIO=y
CONFIG_INIT_ON_ALLOC_DEFAULT_ON=y
CONFIG_ISO9660_FS=y
CONFIG_JOLIET=y
CONFIG_LRU_GEN=y
CONFIG_MODULE_UNLOAD=y
CONFIG_VFAT_FS=y
CONFIG_XFS_FS=y
CONFIG_XFS_ONLINE_SCRUB=y
CONFIG_XFS_POSIX_ACL=y
CONFIG_XFS_QUOTA=y
CONFIG_XFS_RT=y
EOF

  cat <<EOF > $TEST_DIR/config.aarch64
CONFIG_ACPI=y
# for some reasons, aarch64 enables DEBUG_INFO_REDUCED which prevents BTF
CONFIG_DEBUG_INFO_REDUCED=n
CONFIG_FTRACE=y
EOF

  vng --verbose --kconfig --config $TEST_DIR/config --config $TEST_DIR/config.lima --config $TEST_DIR/config.aarch64 --config $TEST_DIR/config.common

  make -j ${FDO_CI_CONCURRENT} ${TARGET}

  COMMAND="mv ${PREP_KERNEL_CHECKOUT}/${BZIMAGE} ${PREP_KERNEL_CHECKOUT}/${BZIMAGE}.selftests"
  echo $COMMAND
  $COMMAND

  COMMAND="mv ${PREP_KERNEL_CHECKOUT}/.config ${PREP_KERNEL_CHECKOUT}/selftests.config"
  echo $COMMAND
  $COMMAND

  exit 0
fi


# now build for USB testing
cat <<EOF >> $TEST_DIR/config
CONFIG_USB_ANNOUNCE_NEW_DEVICES=yq
CONFIG_USB_XHCI_HCD=y
CONFIG_USB_EHCI_HCD=y
CONFIG_USB_UHCI_HCD=y
CONFIG_USB_OHCI_HCD=y
CONFIG_HID_LOGITECH=y
CONFIG_HID_LOGITECH_DJ=y
CONFIG_HID_LOGITECH_HIDPP=y
CONFIG_HID_CP2112=y
CONFIG_I2C=y
CONFIG_I2C_HID=y
CONFIG_I2C_HID_ACPI=y
CONFIG_I2C_HID_PLATFORM=y
CONFIG_GPIOLIB=y
CONFIG_HID_RMI=y
EOF

if [ "$modules" == "yes" ]
then
  # now grep all HID_* configs from Kconfig and turned them as module

  grep --no-group-separator -A1 -E "^config HID_" ${PREP_KERNEL_CHECKOUT}/drivers/hid/Kconfig | \
    sed -e 's/config \(.*\)/CONFIG_\1=/' \
        -e 's/.*tristate.*/m/' -e 's/.*bool.*/y/' | \
    tr -d '\n' | \
    sed -e 's/=\([ym]\)/=\1\n/g' \
     >> $TEST_DIR/config
fi

vng --verbose --kconfig --skip-module --config $TEST_DIR/config
make -j ${FDO_CI_CONCURRENT} ${TARGET}

COMMAND="mv ${PREP_KERNEL_CHECKOUT}/${BZIMAGE} ${PREP_KERNEL_CHECKOUT}/${BZIMAGE}.usb"
echo $COMMAND
$COMMAND

COMMAND="mv ${PREP_KERNEL_CHECKOUT}/.config ${PREP_KERNEL_CHECKOUT}/usb.config"
echo $COMMAND
$COMMAND

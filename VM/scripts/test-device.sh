#!/bin/env bash

set -x
set -e

udevadm info EVENT_PATH | cat

# make sure the device gets correctly enumerated
udevadm info EVENT_PATH | CHECK_UDEVADM

# check if kernel is tainted
if [[ "$(cat /proc/sys/kernel/tainted)" -gt 0 ]]
then
  echo tainted kernel
  exit 1
fi

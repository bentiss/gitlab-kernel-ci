#!/bin/env python3

import logging
import os
import subprocess
import sys

from git_pw import api
from git_pw import config
from git_pw import utils

from slugify import slugify

LOG = logging.getLogger(__name__)


def branch_exists(name):
    """Execute git-branch --list name."""
    cmd = ['git', 'branch', '--list', name]

    try:
        return bool(subprocess.check_output(cmd, stderr=subprocess.STDOUT))
    except subprocess.CalledProcessError as exc:
        print(exc.output)
        sys.exit(exc.returncode)


def git_checkout(*args):
    """Execute git-checkout."""
    cmd = ['git', 'checkout']

    if args:
        cmd.extend(args)

    try:
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as exc:
        print(exc.output)
        sys.exit(exc.returncode)


def git_pull(*args):
    """Execute git-pull."""
    cmd = ['git', 'pull']

    if args:
        cmd.extend(args)

    try:
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        print(output.decode())
    except subprocess.CalledProcessError as exc:
        print(exc.output)
        sys.exit(exc.returncode)


def git_push(*args):
    """Execute git-push."""
    cmd = ['git', 'push']

    if args:
        cmd.extend(args)

    try:
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        print(output.decode())
    except subprocess.CalledProcessError as exc:
        print(exc.output)
        sys.exit(exc.returncode)

def git_pwid(*args):
    """Execute git-pwid."""
    cmd = ['git', 'pwid']

    if args:
        cmd.extend(args)

    try:
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        print(output.decode())
    except subprocess.CalledProcessError as exc:
        print(exc.output)
        sys.exit(exc.returncode)


def ssh_agent(*args):
    """Execute ssh-agent."""
    cmd = ['ssh-agent']

    if args:
        cmd.extend(args)
    else:
        cmd.extend(['-c'])

    try:
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as exc:
        print(exc.output)
        sys.exit(exc.returncode)
    else:
        if not args:
            lines = output.decode().split(';\n')
            for l in lines:
                if not l.startswith('setenv '):
                    continue

                setenv, var, value = l.split()
                os.environ[var] = value


def ssh_add(*args):
    """Execute ssh-add."""
    cmd = ['ssh-add']

    if args:
        cmd.extend(args)

    try:
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        print(output.decode())
    except subprocess.CalledProcessError as exc:
        print(exc.output)
        sys.exit(exc.returncode)


def apply_and_push_series(series):
    try:
        submitter = slugify(series['submitter']['name'])
    except TypeError:
        submitter = slugify(series['submitter']['email'])

    if not series['received_all']:
        return

    branch = f"CI/{series['id']}/{submitter}/{slugify(series['name'])}"
    if branch_exists(branch):
        return

    print(f"applying {series['received_total']} patch(s) to {branch}.")

    try:
        git_checkout('master')
        git_pull()
        git_checkout('-b', branch)
        mbox = api.download(series['mbox'])

        try:
            utils.git_am(mbox, ['--signoff',])
        except SystemExit:
            utils.git_am('--abort', None)
            raise
        os.remove(mbox)

        git_pwid(f"{series['id']}")

        git_push('gitlab-ci', branch)
    finally:
        git_checkout('master')


def process():
    CONF = config.CONF
    CONF.token = utils.git_config('pw.token')
    CONF.server = utils.git_config('pw.server')
    CONF.project = utils.git_config('pw.project')

    series = api.index('series', [('per_page', 10), ('order', '-date')])

    ssh_agent()

    try:
        ssh_add('/home/btissoir/.ssh/id_rsa_pwbot')
        for s in series:
            apply_and_push_series(s)
    finally:
        ssh_agent('-k')

    print('done checking for new patches on the list')


if __name__ == '__main__':
    process()

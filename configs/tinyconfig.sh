#!/bin/bash

CI_PIPELINE_ID=$1

set -xe

make tinyconfig

# enable basic INPUT USB_SUPPORT I2C and modules support
sed -i -E 's/^# CONFIG_(INPUT|MODULES|USB_SUPPORT|I2C) is not set/CONFIG_\1=y/' .config

cat <<EOF >> .config
CONFIG_ENABLE_MUST_CHECK=y
CONFIG_DEBUG_FS=y
CONFIG_HEADERS_CHECK=y
CONFIG_DEBUG_SECTION_MISMATCH=y

# for hid-playstation
CONFIG_NEW_LEDS=y
CONFIG_LEDS_CLASS=y
CONFIG_LEDS_CLASS_MULTICOLOR=y

CONFIG_USB=y

CONFIG_UHID=m
CONFIG_USB_HID=m
CONFIG_I2C_HID=m
CONFIG_HIDRAW=y
CONFIG_HID_BATTERY_STRENGTH=y
CONFIG_HID_GENERIC=m
CONFIG_USB_HIDDEV=y
CONFIG_USB_KBD=m
CONFIG_USB_MOUSE=m

CONFIG_INPUT_EVDEV=y

CONFIG_LOCALVERSION="-CI-PIPELINE-$CI_PIPELINE_ID"
EOF

for i in 1 2 3 ;
do
  # check for new CONFIGS
  make yes2modconfig

  # switch all HID to modules
  sed -i -E 's/^# CONFIG_(U|I2C_|USB_)?HID(.*) is not set/CONFIG_\1HID\2=y/' .config

  # force the HID_FF modules to be set
  sed -i -E 's/^# CONFIG_(.*_FF) is not set/CONFIG_\1=y/' .config
done

make olddefconfig
